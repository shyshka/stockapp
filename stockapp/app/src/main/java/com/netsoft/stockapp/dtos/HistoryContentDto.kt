package com.netsoft.stockapp.dtos

data class HistoryContentDto(
    val quoteSymbols: List<QuoteSymbolDto>
)

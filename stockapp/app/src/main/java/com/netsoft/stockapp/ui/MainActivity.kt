package com.netsoft.stockapp.ui

import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.netsoft.stockapp.R
import com.netsoft.stockapp.models.CandleChartPoint
import com.netsoft.stockapp.models.LineChartPoint
import com.netsoft.stockapp.models.PeriodEnum
import com.netsoft.stockapp.utils.ChartUtils.DateDayMonthFormatter
import com.netsoft.stockapp.viewmodels.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val mViewModel: MainActivityViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViewModel()

        initViews()

        loadData()
    }

    private fun initViews() {
        initCheckBoxCombined()

        initCheckBoxCandle()

        initRadioGroupPeriod()

        initChart()
    }

    private fun initCheckBoxCandle() {
        cbox_candle.setOnCheckedChangeListener { _, isChecked ->
            mViewModel.setIsCandleMode(isChecked)
            if (isChecked && cbox_combined.isChecked) {
                cbox_combined.isChecked = false
            }
        }
    }

    private fun initCheckBoxCombined() {
        cbox_combined.setOnCheckedChangeListener { _, isChecked ->
            mViewModel.setIsCombinedMode(isChecked)
            radio_group_symbols.children.forEach { rBtn ->
                rBtn.isEnabled = !isChecked
            }
        }
    }

    private fun initRadioGroupPeriod() {
        radio_group_period.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                radio_btn_month.id -> mViewModel.setSelectedPeriod(PeriodEnum.Month)
                radio_btn_week.id -> mViewModel.setSelectedPeriod(PeriodEnum.Week)
            }
        }

        radio_group_symbols.setOnCheckedChangeListener { group, checkedId ->
            mViewModel.setSelectedSymbol((group.children.find { view -> view.id == checkedId } as RadioButton).text.toString())
        }
    }

    private fun initViewModel() {

        mViewModel.isCandleMode.observe(this, {
            if (cbox_candle.isChecked != it) {
                cbox_candle.isChecked = it
            }
        })

        mViewModel.isCombineMode.observe(this, {
            if (cbox_combined.isChecked != it) {
                cbox_combined.isChecked = it
            }
        })

        mViewModel.availableSymbolsLD.observe(this, { list ->

            radio_group_symbols.removeAllViews()

            list.forEachIndexed { index, s ->
                val radioBtn = RadioButton(this)
                radioBtn.text = s
                radioBtn.id = 1000 + index
                radio_group_symbols.addView(radioBtn)
            }

            if (radio_group_symbols.checkedRadioButtonId < 0) {
                radio_group_symbols.check(radio_group_symbols.children.first().id)
            }

        })

        mViewModel.selectedSymbolLD.observe(this, {
            val radioButton = radio_group_symbols.children.find {
                (it as RadioButton).text == it
            }

            radioButton?.let { rBtn ->
                if (radio_group_symbols.checkedRadioButtonId != rBtn.id) {
                    radio_group_symbols.check(rBtn.id)
                }
            }
        })

        mViewModel.selectedPeriodLD.observe(this, { period ->

            val rBtnId = when (period!!) {
                PeriodEnum.Month -> radio_btn_month.id
                PeriodEnum.Week -> radio_btn_week.id
            }

            if (radio_group_period.checkedRadioButtonId != rBtnId) {
                radio_group_period.check(rBtnId)
            }

        })

        mViewModel.chartLinesDataLD.observe(this, {
            updateLineCharts(it)
        })

        mViewModel.candleChartPointsLD.observe(this, {
            updateCandleChart(it.first, it.second)
        })
    }

    @SuppressLint("NewApi")
    private fun updateLineCharts(map: Map<String, List<LineChartPoint>>?) {
        line_chart.visibility = View.VISIBLE
        candle_stick_chart.visibility = View.GONE

        val lineData = LineData()

        map?.forEach { symbol, chartPoints ->
            val chartEntries = ArrayList<Entry>()

            chartPoints.forEach { point ->
                chartEntries.add(Entry(point.timestamp.toFloat(), point.performance.toFloat()))
            }

            val dataSet = LineDataSet(chartEntries, symbol)
            dataSet.run {
                lineWidth = 3f
                color = ColorTemplate.MATERIAL_COLORS[map.keys.indexOf(symbol)]
                setCircleColor(ColorTemplate.JOYFUL_COLORS[map.keys.indexOf(symbol)])
                circleRadius = 4f
                setDrawCircleHole(false)
                valueTextSize = 10f
                valueFormatter = PercentFormatter()
            }

            lineData.addDataSet(dataSet)
        }

        line_chart.run {
            clear()
            data = lineData
            animateXY(500, 500)
            invalidate()
        }
    }

    private fun updateCandleChart(symbol: String, list: List<CandleChartPoint>) {
        line_chart.visibility = View.GONE
        candle_stick_chart.visibility = View.VISIBLE

        val candleEntries = mutableListOf<CandleEntry>()

        list.forEach {
            candleEntries.add(
                CandleEntry(
                    it.timestamp.toFloat(),
                    it.high.toFloat(),
                    it.low.toFloat(),
                    it.open.toFloat(),
                    it.close.toFloat()
                )
            )
        }

        val dataSet = CandleDataSet(candleEntries, symbol)
        dataSet.shadowColor = getColor(R.color.black)
        dataSet.shadowWidth = 2f
        dataSet.decreasingColor = getColor(R.color.red_700)
        dataSet.decreasingPaintStyle = Paint.Style.FILL
        dataSet.increasingColor = getColor(R.color.green_700)
        dataSet.increasingPaintStyle = Paint.Style.FILL

        val candleData = CandleData(dataSet)

        candle_stick_chart.run {
            clear()
            data = candleData
            animateXY(500, 500)
            invalidate()
        }
    }

    private fun initChart() {
        line_chart.run {
            description.isEnabled = false

            axisLeft.labelCount = 10
            axisLeft.valueFormatter = PercentFormatter()

            axisRight.labelCount = 10
            axisRight.valueFormatter = PercentFormatter()

            xAxis.valueFormatter = DateDayMonthFormatter()
            xAxis.labelCount = 10
        }
    }

    private fun loadData() {
        mViewModel.loadAvailableSymbols()
        mViewModel.loadStockData()
    }

}
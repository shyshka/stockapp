package com.netsoft.stockapp.dtos

data class QuoteSymbolDto(
    val symbol: String,
    val timestamps: List<Long>,
    val opens: List<Double>,
    val closures: List<Double>,
    val highs: List<Double>,
    val lows: List<Double>,
    val volumes: List<Long>
)

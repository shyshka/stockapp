package com.netsoft.stockapp.models

data class LineChartPoint(
    val timestamp: Long,
    val performance: Double
)
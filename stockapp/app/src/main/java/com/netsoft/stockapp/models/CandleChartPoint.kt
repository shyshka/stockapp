package com.netsoft.stockapp.models

data class CandleChartPoint(
    val timestamp: Long,
    val high: Double,
    val low: Double,
    val open: Double,
    val close: Double
)
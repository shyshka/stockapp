package com.netsoft.stockapp.services

import android.content.Context
import com.google.gson.Gson
import com.netsoft.stockapp.R
import com.netsoft.stockapp.dtos.HistoryDto
import com.netsoft.stockapp.dtos.QuoteSymbolDto
import com.netsoft.stockapp.utils.AssetUtils

class StockRepository(private val mContext: Context) {

    fun getAvailableSymbols(): List<String> {

        val result = mutableListOf<String>()

        val monthHistory = getFullMonthHistory()
        monthHistory?.let { history ->
            history.content.quoteSymbols.forEach {
                result.add(it.symbol)
            }
        }

        return result
    }

    fun getFullMonthHistory(): HistoryDto? {
        return try {
            val json = AssetUtils.loadTextFromAsset(
                mContext,
                mContext.getString(R.string.asset_name_history_month)
            )
            Gson().fromJson(json, HistoryDto::class.java)
        } catch (ex: Exception) {
            ex.printStackTrace()
            null
        }
    }

    fun getFullWeekHistory(): HistoryDto? {
        return try {
            val json = AssetUtils.loadTextFromAsset(
                mContext,
                mContext.getString(R.string.asset_name_history_week)
            )
            Gson().fromJson(json, HistoryDto::class.java)
        } catch (ex: Exception) {
            ex.printStackTrace()
            null
        }
    }

    fun getWeekSymbolHistory(symbol: String): QuoteSymbolDto? {
        val monthHistory = getFullWeekHistory()
        return monthHistory?.content?.quoteSymbols?.find { it.symbol == symbol }
    }

    fun getMonthSymbolHistory(symbol: String): QuoteSymbolDto? {
        val monthHistory = getFullMonthHistory()
        return monthHistory?.content?.quoteSymbols?.find { it.symbol == symbol }
    }

}
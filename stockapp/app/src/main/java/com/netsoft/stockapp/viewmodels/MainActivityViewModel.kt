package com.netsoft.stockapp.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.netsoft.stockapp.dtos.QuoteSymbolDto
import com.netsoft.stockapp.models.CandleChartPoint
import com.netsoft.stockapp.models.LineChartPoint
import com.netsoft.stockapp.models.PeriodEnum
import com.netsoft.stockapp.services.StockRepository
import org.koin.core.KoinComponent
import org.koin.core.inject

class MainActivityViewModel : ViewModel(), KoinComponent {

    private val mStockRepository: StockRepository by inject()

    val availableSymbolsLD = MutableLiveData<List<String>>()

    val selectedSymbolLD = MutableLiveData<String>()

    val selectedPeriodLD = MutableLiveData<PeriodEnum>()

    val isCombineMode = MutableLiveData<Boolean>()

    val isCandleMode = MutableLiveData<Boolean>()

    val chartLinesDataLD = MutableLiveData<Map<String, List<LineChartPoint>>>()

    val candleChartPointsLD = MutableLiveData<Pair<String, List<CandleChartPoint>>>()

    init {
        selectedPeriodLD.value = PeriodEnum.Month
        isCombineMode.value = false
        isCandleMode.value = false
    }

    fun loadStockData() {
        isCandleMode.value?.let { isCandle ->
            if (isCandle) {
                loadCandleDataBySelectedSymbol()
                return
            }
        }

        isCombineMode.value?.let {
            if (it) {
                loadStockDataAsCombined()
            } else {
                loadStockDataBySelectedSymbol()
            }
        }
    }

    private fun loadCandleDataBySelectedSymbol() {
        selectedSymbolLD.value?.let { symbol ->
            val stockHistory = getStockHistoryBySymbol(symbol)
            val candleChartPoint = getCandleChartPoints(stockHistory)
            candleChartPointsLD.postValue(Pair(symbol, candleChartPoint))
        }
    }

    private fun loadStockDataBySelectedSymbol() {
        selectedSymbolLD.value?.let {
            val stockHistory = getStockHistoryBySymbol(it)
            val map = mutableMapOf<String, List<LineChartPoint>>()
            map[it] = getLineChartPoints(stockHistory)
            chartLinesDataLD.postValue(map)
        }
    }

    private fun loadStockDataAsCombined() {

        val map = mutableMapOf<String, List<LineChartPoint>>()

        mStockRepository.getAvailableSymbols().forEach {
            val stockHistory = getStockHistoryBySymbol(it)
            map[it] = getLineChartPoints(stockHistory)
        }

        chartLinesDataLD.postValue(map)
    }

    private fun getStockHistoryBySymbol(symbol: String): QuoteSymbolDto? {

        selectedPeriodLD.value?.let {
            return when (it) {
                PeriodEnum.Month -> {
                    mStockRepository.getMonthSymbolHistory(symbol)
                }

                PeriodEnum.Week -> {
                    mStockRepository.getWeekSymbolHistory(symbol)
                }
            }
        }

        return null
    }

    private fun getCandleChartPoints(stockHistory: QuoteSymbolDto?): List<CandleChartPoint> {
        val candleData = mutableListOf<CandleChartPoint>()

        stockHistory?.run {

            timestamps.forEachIndexed { index, timestamp ->

                candleData.add(
                    CandleChartPoint(
                        index.toLong(),
                        highs[index],
                        lows[index],
                        opens[index],
                        closures[index]
                    )
                )
            }
        }

        return candleData
    }

    private fun getLineChartPoints(stockHistory: QuoteSymbolDto?): List<LineChartPoint> {
        val chartData = mutableListOf<LineChartPoint>()

        stockHistory?.run {

            val startPrice = (highs[0] + lows[0]) / 2

            timestamps.forEachIndexed { index, timestamp ->
                val price = (highs[index] + lows[index]) / 2

                val difPrice = startPrice - price

                val performance = difPrice * 100.0 / startPrice

                chartData.add(LineChartPoint(timestamp, performance))
            }
        }

        return chartData
    }

    fun loadAvailableSymbols() {
        availableSymbolsLD.postValue(mStockRepository.getAvailableSymbols())
    }

    fun setSelectedPeriod(period: PeriodEnum) {
        selectedPeriodLD.value = period
        loadStockData()
    }

    fun setSelectedSymbol(value: String) {
        selectedSymbolLD.value = value
        loadStockData()
    }

    fun setIsCombinedMode(checked: Boolean) {
        isCombineMode.value = checked
        loadStockData()
    }

    fun setIsCandleMode(checked: Boolean) {
        isCandleMode.value = checked
        loadStockData()
    }

}
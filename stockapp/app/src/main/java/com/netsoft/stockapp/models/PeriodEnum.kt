package com.netsoft.stockapp.models

enum class PeriodEnum {
    Month,
    Week
}
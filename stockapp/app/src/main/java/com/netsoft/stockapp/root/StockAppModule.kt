package com.netsoft.stockapp.root

import com.netsoft.stockapp.services.StockRepository
import com.netsoft.stockapp.viewmodels.MainActivityViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val stockAppModule = module {
    single { StockRepository(get()) }
    viewModel { MainActivityViewModel() }
}

package com.netsoft.stockapp.utils

import android.annotation.SuppressLint
import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.SimpleDateFormat
import java.util.*

object ChartUtils {

    fun DateDayMonthFormatter(): ValueFormatter {
        return object : ValueFormatter() {
            @SuppressLint("SimpleDateFormat")
            override fun getFormattedValue(value: Float): String {
                val date = Date((value * 1000).toLong())
                return SimpleDateFormat("dd MMM").format(date)
            }
        }
    }


}
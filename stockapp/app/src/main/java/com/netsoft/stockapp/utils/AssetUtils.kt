package com.netsoft.stockapp.utils

import android.content.Context
import java.io.InputStreamReader

object AssetUtils {

    fun loadTextFromAsset(context: Context, assetName: String): String? {
        return try {
            val stream = context.assets.open(assetName)
            val streamReader = InputStreamReader(stream)
            streamReader.readText()
        } catch (ex: Exception) {
            ex.printStackTrace()
            null
        }
    }
}
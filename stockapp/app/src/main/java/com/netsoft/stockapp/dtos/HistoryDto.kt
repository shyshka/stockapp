package com.netsoft.stockapp.dtos

data class HistoryDto(
    val content: HistoryContentDto,
    val status: String
)